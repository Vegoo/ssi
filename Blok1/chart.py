import matplotlib.pyplot as plt

def takePosition(position, listName):
    position = position - 1
    result = []
    for i in range(len(listName)):
        a = []
        for j in range(len(listName[0])):
            a.append(float(listName[i][j][position]))
        result.append(a)
    return result


def createChart(a, b, list, xlabel, ylabel):
    for i in range(len(a)):
        plt.scatter(a[i], b[i], label=list[i],
                     marker='o')
    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()

