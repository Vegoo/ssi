import math
import matplotlib.pyplot as plt

def miara_niepodobienstwa(listaA, listaB):
    miara = 0
    for i in range(len(listaA)):
        for j in range(len(listaA[0])):
            odl_min = 999999
            if listaA[i][j] == 0:
                continue
            for k in range(len(listaB)):
                for l in range(len(listaB[0])):
                    if listaB[k][l] == 0:
                        continue
                    odl_obecna = math.fabs(i - k) + math.fabs(j - l)
                    #if odl_min > odl_obecna:
                    #    odl_min = odl_obecna
                    odl_min = min(odl_min, odl_obecna)
            miara = miara + odl_min
    return miara

def znajd(lista):
    pod = -99999
    for i in range(len(listaWzorcow)):
        podobienstwo = -1 * (
                    miara_niepodobienstwa(lista, listaWzorcow[i]) + miara_niepodobienstwa(listaWzorcow[i],
                                                                                                 lista))
        if pod < podobienstwo:
            pod = podobienstwo
            obecny = i
    return obecny

def rysuj(lista):
    x = []
    y = []
    for i in range(len(lista)):
        for j in range(len(lista[0])):
            if lista[i][j] == 1:
                x.append(i)
                y.append(j)
    plt.plot(x, y)
    plt.show()

listaWzorcow = []

listaWzorcowa1 = [[0, 0, 0, 1],
                 [0, 0, 1, 1],
                 [0, 1, 0, 1],
                 [0, 0, 0, 1],
                 [0, 0, 0, 1]]

listaWzorcowa2 = [[0, 1, 1, 1],
                 [1, 0, 0, 1],
                 [0, 0, 1, 0],
                 [0, 1, 0, 0],
                 [1, 1, 1, 1]]

listaWzorcowa3 = [[1, 1, 1, 0],
                 [0, 0, 0, 1],
                 [1, 1, 1, 1],
                 [0, 0, 0, 1],
                 [1, 1, 1, 0]]

listaWzorcow.append(listaWzorcowa1)
listaWzorcow.append(listaWzorcowa2)
listaWzorcow.append(listaWzorcowa3)










