from tkinter import *
import functools
import numpy as np
import zad1
import copy

root = Tk()
root.geometry("350x500")
pixele = np.zeros((5, 4), dtype=int)
pixele = pixele.tolist()


def dodajwzorzec():
    narazie = copy.deepcopy(pixele)
    zad1.listaWzorcow.append(narazie)


def wzorcowe():
    for a in range(len(zad1.listaWzorcow)):
        newWindow = Toplevel(root)
        newWindow.title("wzorzec " + str(a))
        newWindow.geometry("300x500")
        for i in range(4):
            for j in range(5):
                label = Label(newWindow, text=str(j) + str(i), bg='#ffffff', width='6', height='3')
                if zad1.listaWzorcow[a][j][i] == 1:
                    label.config(bg='#000000')
                label.place(x=30 + (i * 60), y=30 + (j * 60))


def dopasowanie():
    wynik = zad1.znajd(pixele)
    result.config(text="najbardziej podobny jest do wzorca o numerze " + str(wynik))


def changecolor(event, par):
    if par['background'] == "#ffffff":
        par.config(bg="#000000")
        pixele[int(par['text'][0])][int(par['text'][1])] = 1
    else:
        par.config(bg="#ffffff")
        pixele[int(par['text'][0])][int(par['text'][1])] = 0


for i in range(4):
    for j in range(5):
        label = Label(root, text=str(j) + str(i), bg='#ffffff', width='6', height='3')
        label.bind("<Button-1>", functools.partial(changecolor, par=label))
        label.place(x=30 + (i * 60), y=30 + (j * 60))

result = Label(root, text="wynik")
result.place(x='50', y='400')

button = Button(root, text='Zachlanne dopasowanie', command=dopasowanie)
button.place(x='10', y='450')

button1 = Button(root, text='wyswietl bitmapy wzorcowe', command=wzorcowe)
button1.place(x='180', y='450')

button2 = Button(root, text="dodaj jako wzorzec", command=dodajwzorzec)
button2.place(x='50', y='330')

root.mainloop()
