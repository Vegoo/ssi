import numpy as np
import matplotlib.pyplot as plt
import copy

def zmian(obrazek):
    obraz = []
    for i in range(len(obrazek)):
        for j in range(len(obrazek[0])):
            if obrazek[i][j] == 0:
                obraz.append(-1)
            else:
                obraz.append(1)
    return obraz

def naListe(lista):
    lis = []
    for i in range(5):
        wiersz = []
        for j in range(5):
            if lista[j + (i * 5)] == 1:
                wiersz.append(1)
            else:
                wiersz.append(0)
        lis.append(wiersz)
    return lis


class HopfieldNN:
    def __init__(self, height, width):
        n = height * width

        self.weights = np.zeros((n, n))

    def train_image(self, img):
        img = zmian(img)
        for i in range(len(img)):
            for j in range(len(img)):
                if i == j:
                    continue
                self.weights[i][j] = self.weights[i][j] + img[i] * img[j] * 1 / 25

    def recognize_image(self, img):
        obraz = zmian(img)
        #obrazDoSprawdzenia = copy.deepcopy(obraz)
        for i in range(25):
            naprawiany = obraz
            suma = 0
            for j in range(25):
                suma = suma + naprawiany[j] * self.weights[i][j]
            if suma >= 0:
                 naprawiany[i] = 1
            else:
                 naprawiany[i] = -1
        #naprawiony = []
        #for i in range(5):
            #wiersz = []
            #for j in range(5):
                #wiersz.append(naprawiany[j + (i * 5)])
            #naprawiony.append(wiersz)
        return naprawiany
        #return False



Hop = HopfieldNN(5, 5)

Obraz1 = [[1, 1, 0, 0, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0]]
Obraz2 = [[1, 0, 0, 0, 1], [0, 1, 0, 1, 0], [0, 0, 1, 0, 0], [0, 1, 0, 1, 0], [1, 0, 0, 0, 1]]
Obraz3 = [[0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [1, 1, 1, 1, 1], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]]

listaWzorcow = []
listaWzorcow.append(Obraz1)
listaWzorcow.append(Obraz2)
listaWzorcow.append(Obraz3)

test3 = [[0, 0, 0, 0, 0] ,[0, 0, 1, 0, 0],[1, 1, 1, 1, 1],[0, 0, 0, 0, 0],[0, 0, 1, 0, 0]]
test2 = [[1, 1, 0, 0, 1] ,[0, 1, 0, 1, 0],[0, 1, 1, 1, 0],[0, 1, 0, 1, 0],[1, 1, 0, 0, 1]]
test1 = [[0, 1, 0, 0, 0] ,[0, 1, 0, 0, 0],[0, 1, 0, 0, 0],[0, 1, 0, 0, 0],[0, 1, 0, 0, 0]]
test4 = [[0, 1, 1, 1, 1] ,[1, 0, 1, 1, 1],[1, 0, 1, 1, 1],[1, 0, 1, 1, 1],[1, 0, 1, 1, 1]]
test = [[0, 0, 0, 0, 1] ,[0, 0, 0, 0, 1],[0, 0, 0, 0, 1],[0, 0, 0, 0, 1],[1, 0, 0, 0, 1]]

#test1 = [[1, 1, 0, 0, 1] ,
#        [0, 1, 0, 1, 0],
#        [0, 0, 1, 0, 0],
#        [0, 1, 0, 1, 0],
#        [1, 0, 0, 0, 1]]
#test3 = [[1, 0, 0, 0, 1], [0, 1, 0, 1, 0], [0, 0, 1, 0, 0], [0, 1, 0, 1, 0], [1, 0, 0, 0, 1]]

Hop.train_image(Obraz1)
Hop.train_image(Obraz2)
Hop.train_image(Obraz3)

#fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3)
#ax1.set_title("testowa")
#ax1.imshow(test4, cmap="Greys")

#a = 0
#while True:
#    if a == 0:
#        sprawdz = copy.deepcopy(test4)
#        cos = Hop.recognize_image(test4)
#    else:
#        cos = Hop.recognize_image(sprawdz)
#    cos = naListe(cos)
#    if sprawdz == cos:
#        break
#    a = 1
#    sprawdz = copy.deepcopy(cos)



#ax3.set_title("testowa po naprawie")
#ax3.imshow(cos, cmap="Greys")
#ax4.set_title("wzorcowa 0")
#ax4.imshow(Obraz1, cmap='Greys')
#ax5.set_title("wzorcowa 1")
#ax5.imshow(Obraz2, cmap='Greys')
#ax6.set_title("wzorcowa 2")
#ax6.imshow(Obraz3, cmap='Greys')
#plt.show()