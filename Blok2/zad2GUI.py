from tkinter import *
import functools
import numpy as np
import zad2
import copy

root = Tk()
root.geometry("350x500")
pixele = np.zeros((5, 5), dtype=int)
pixele = pixele.tolist()
def wzorcowe():
    for a in range(len(zad2.listaWzorcow)):
        newWindow = Toplevel(root)
        newWindow.title("wzorzec "+str(a))
        newWindow.geometry("350x500")
        for i in range(5):
            for j in range(5):
                label = Label(newWindow, text=str(j) + str(i), bg='#ffffff', width='6', height='3')
                if zad2.listaWzorcow[a][j][i] == 1:
                    label.config(bg='#000000')
                label.place(x=30 + (i * 60), y=30 + (j * 60))

def napraw():
    global pixele
    newWindow = Toplevel(root)
    newWindow.title('mapa testowa po naprawie')
    newWindow.geometry("350x500")

    a = 0
    sprawdz = []
    while True:
        if a == 0:
            sprawdz = copy.deepcopy(pixele)
            cos = zad2.Hop.recognize_image(pixele)
        else:
            cos = zad2.Hop.recognize_image(sprawdz)
        cos = zad2.naListe(cos)
        if sprawdz == cos:
            break
        a = 1
        sprawdz = copy.deepcopy(cos)

    for i in range(5):
        for j in range(5):
            label = Label(newWindow, text=str(j) + str(i), bg='#ffffff', width='6', height='3')
            if cos[j][i] == 1:
                label.config(bg='#000000')
            label.place(x=30 + (i * 60), y=30 + (j * 60))

def changecolor(event, par):
    if par['background'] == "#ffffff":
        par.config(bg="#000000")
        pixele[int(par['text'][0])][int(par['text'][1])] = 1
    else:
        par.config(bg="#ffffff")
        pixele[int(par['text'][0])][int(par['text'][1])] = 0


for i in range(5):
    for j in range(5):
        label = Label(root, text=str(j)+str(i), bg='#ffffff', width='6', height='3')
        label.bind("<Button-1>", functools.partial(changecolor, par=label))
        label.place(x=30 + (i * 60), y=30 + (j * 60))


button = Button(root, text='napraw', command=napraw)
button.place(x='10', y='450')

button1 = Button(root, text='wyswietl bitmapy wyuczone', command=wzorcowe)
button1.place(x='180', y='450')



root.mainloop()
